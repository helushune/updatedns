#!/usr/bin/env bash

# https://gitlab.com/helushune/updatedns

# requirements:
# dig, curl, base64 (for change alert emails), nsupdate (bind-utils/samba-nsupdate)

# usage:
# updatedns.sh [A | AAAA] [host-to-update]


TSIGKEY="/path/to/dns-tsig.key"				# tsig-keygen -a hmac-sha512/hmac-sha256 ddnsupdatekey
DIGHOST="9.9.9.9"					# DNS server to query for current DNS record
DNSZONE="name.tld."					# DNS zone expecting the DDNS update.  Varies depending on DNS server configuration.
DATE=$(date +"%m_%d_%Y")
RECORDTYPE=$1
COMPAREHOST=$2
TEMPFILE="/tmp/updatedns_${COMPAREHOST}_${DATE}"	# Temporary file to store commands to send to nsupdate.  This shouldn't need changing.
LOGFILE="/tmp/updatedns_${COMPAREHOST}_${DATE}.log"	# Debug output of nsupdate in case anything goes wrong
NSUPDATESERVER="ns.name.tld|x.x.x.x"			# Server to send the Dynamic DNS update to
DNSTTL="1800"						# TTL to send with dynamic dns zone
EMAILENABLED=0						# Set to 1 to send a change summary email
SMTPUSER="alerts@domain.tld"				# Email user to send change email as, if above is enabled
SMTPPASSWORD="secret"					# Password for above user
SMTPSERVER="smtp.domain.tld"				# Email server to use
MAILFROM="alerts@domain.tld"				# From address to use.  In most cases, this will be the same as SMTPUSER.
MAILTO="admin@domain.tld"				# Address to send summary email to
ECHO=$(which echo)
NSUPDATE=$(which nsupdate)
CURL=$(which curl)
DIG=$(which dig)


do_output() {
  $ECHO "Variables:"
  $ECHO "Check URL - ${CHECKURL}"
  $ECHO "Current IP - ${CURRENTIP}"
  $ECHO "Current DNS entry - ${RECORDIP}"
  $ECHO "Dig host - ${DIGHOST}"
  $ECHO "TSIG key file - ${TSIGKEY}"
  $ECHO "Record Type - ${RECORDTYPE}"
  $ECHO "Compare host - ${COMPAREHOST}"
  $ECHO "nsupdate server - ${NSUPDATESERVER}"
  $ECHO "DNS zone name - ${DNSZONE}"
  $ECHO "DNS TTL - ${DNSTTL}"
  $ECHO "Temp file - ${TEMPFILE}"
  $ECHO "nsupdate - ${NSUPDATE}"
  $ECHO "echo - ${ECHO}"
  $ECHO "curl - ${CURL} ${CURLFLAGS}"
  $ECHO "dig - ${DIG}"
  $ECHO "Change alert email - ${EMAILENABLED}"
  if [[ $EMAILENABLED == "1"]]; then
    $ECHO "SMTP server - ${SMTPSERVER}"
    $ECHO "SMTP user - ${SMTPUSER}"
    $ECHO "Mail to - ${MAILTO}"
    $ECHO "Mail from - ${MAILFROM}"
    $ECHO "SMTP user base64 - ${SMTPUSERB64}"
    $ECHO "Email hostname - ${EMAILHOSTNAME}"
    $ECHO "Mail subject - ${MAILSUBJECT}"
  fi
}

do_setvars() {
  if [[ $RECORDTYPE == "A" ]]; then
    CHECKURL="ifconfig.co"
    CURLFLAGS="-4 -s"
  elif [[ $RECORDTYPE == "AAAA" ]]; then
    CHECKURL="ifconfig.co"
    CURLFLAGS="-6 -s"
  else
    $ECHO "!! Record type must be A or AAAA"
    exit 1
  fi
}

do_getip() {
  $ECHO "Checking external IP..."
  CURRENTIP=$($CURL $CURLFLAGS $CHECKURL)
  if [[ -z "$CURRENTIP" ]]; then
    do_quit_external_fail
  fi
  $ECHO "External IP is $CURRENTIP"
}

do_compareip() {
  $ECHO "Comparing external IP and $RECORDTYPE record for $COMPAREHOST"
  RECORDIP=$(dig "$COMPAREHOST" "$RECORDTYPE" +short @"$DIGHOST")
  $ECHO "$RECORDTYPE record for host $COMPAREHOST is $RECORDIP"
  if [[ -z "$RECORDIP" ]]; then
    do_quit_record_null
  fi
  if [[ $CURRENTIP == "$RECORDIP" ]]; then
#    do_output
    do_quit_no_update
  else
    $ECHO "$RECORDTYPE record update required"
    do_nsupdate
  fi
}

do_quit_record_null() {
  echo "!! Fetched a null value for $RECORDTYPE record for host $COMPAREHOST"
  exit 1
}

do_quit_external_fail() {
  $ECHO "!! Failed to determine external IP"
  exit 1
}

do_quit_no_update() {
  $ECHO "No $RECORDTYPE record update needed..."
  exit
}

do_quit() {
  $ECHO "$RECORDTYPE record update sent for zone $COMPAREHOST"
  exit
}

do_nsupdate() {
  $ECHO "Preparing nsupdate commands"
  $ECHO "server $NSUPDATESERVER" > "$TEMPFILE"
  $ECHO "debug yes" >> "$TEMPFILE"
  $ECHO "zone $DNSZONE" >> "$TEMPFILE"
  $ECHO "update delete $COMPAREHOST $RECORDTYPE" >> "$TEMPFILE"
  $ECHO "update add $COMPAREHOST $DNSTTL $RECORDTYPE $CURRENTIP" >> "$TEMPFILE"
  $ECHO "send" >> "$TEMPFILE"
  $ECHO "Sending $RECORDTYPE update for $COMPAREHOST to $NSUPDATESERVER"
  $NSUPDATE -k ${TSIGKEY} -v "${TEMPFILE}" > "$LOGFILE" 2>&1
  rm -rf "$TEMPFILE"
} 

do_construct_email() {
  SMTPUSERB64=$($ECHO -ne ${SMTPUSER} | base64)  
  SMTPPASSB64=$($ECHO -ne ${SMTPPASSWORD} | base64)    
  EMAILSUBJRECORDTYPE="[${RECORDTYPE}]"
  NEWLINE=$'\012'
  EMAILHOSTNAME=$(hostname)
  MAILSUBJECT="Updated ${EMAILSUBJRECORDTYPE} DNS record for ${COMPAREHOST}"
  HTMLEMAILCONTENT="<pre style=\"font: monospace\">
    Dynamic DNS record for ${COMPAREHOST} have been updated.  Below is a summary of the change-<br><br>
    Domain: ${COMPAREHOST}<br>
    Record type: ${RECORDTYPE}<br>
    Old record: ${RECORDIP}<br>
    New record: ${CURRENTIP}"
  $ECHO -ne "ehlo ${EMAILHOSTNAME}\r\n"
  $ECHO -ne "AUTH LOGIN\r\n"
  $ECHO -ne "${SMTPUSERB64}\r\n"
  $ECHO -ne "${SMTPPASSB64}\r\n"
  $ECHO -ne "MAIL FROM: <${MAILFROM}>\r\n"
  $ECHO -ne "RCPT TO: <${MAILTO}>\r\n"
  $ECHO -ne "DATA\r\n"
  $ECHO -ne "Content-type: text/html\r\n"
  $ECHO -ne "From: <${MAILFROM}>\r\n"
  $ECHO -ne "To: <${MAILTO}>\r\n"
  $ECHO -ne "Subject: ${MAILSUBJECT}\r\n"
  $ECHO -ne "\r\n"
  $ECHO -ne ${HTMLEMAILCONTENT//\\n/$NEWLINE}"\r\n"
  $ECHO -ne ".\r\n"
  $ECHO -ne "quit"
}

do_send_email() {
  do_construct_email | openssl s_client -starttls smtp -connect ${SMTPSERVER}:587 > /dev/null
}

main() {
  do_setvars
  do_getip
  do_compareip
  if [[ $EMAILENABLED == "1" ]]; then
    do_send_email
  fi
# do_output 
  do_quit
}

main "$@"
